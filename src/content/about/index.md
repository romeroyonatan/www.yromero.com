---
title: "About me"
date: 2023-10-29T18:10:14+01:00
draft: false
---

Hi, my name is **Yonatan**, and I am a Software Engineer with over 10 years of
experience developing complex systems.

I have experience with networking protocols, security, Linux, Docker,
Kubernetes, and more.

I am a TDD practitioner who values crafting simple and testable code. I avoid
premature optimizations and unnecessary abstractions.

I frequently refactor code, supported by automated tests for safety.

I offer my services as a **freelance developer** for Python applications and can
also assist with DevOps in your organization. I have experience with GitLab
Pipelines and GitHub Actions.

If you're looking for reliable software development or assistance with your
projects, feel free to reach out. I'm here to help you achieve your goals.

If you're a fellow developer seeking assistance with debugging or just want to
collaborate on a project, don't hesitate to get in touch. I'm always open to
working together and sharing knowledge with the developer community


## Contact
- hello{at}yromero.com
- [github.com/romeroyonatan][1]
- [LinkedIn][4]


## Portfolio

While most of my work remains private (apologies for the limited visibility),
here are a few notable projects:

1. **Security Web Application for ERP Protection**
   - Developed a security web application aimed at safeguarding ERP systems.

2. **Dental Aligners Manufacturing System for [All Right][2]**
   - Collaborated on the software development to support the manufacturing
process for dental aligners at [All Right][2].

3. **Personal Expenses Bot for Telegram**
   - Created a [Telegram bot][3] for managing personal expenses. It's a handy tool
to keep track of your financial activities.

These projects showcase a range of experiences, from security-focused web applications to manufacturing systems and personal finance tools.


[1]: https://www.github.com/romeroyonatan
[2]: https://www.all-right.com.ar/
[3]: https://www.gitlab.com/romeroyonatan/mis-gastos-bot
[4]: http://www.linkedin.com/in/yonatan-r-908b90274
