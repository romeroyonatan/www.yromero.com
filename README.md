Romero's
=========
> Software services


## Requirements
- [Hugo][1]

## Development
```
hugo server --buildDrafts
```

### Create content
```
hugo new content posts/my-first-post.md
```

## Deploy
```
hugo
```

[1]: https://gohugo.io/
